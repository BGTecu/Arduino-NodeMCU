#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <aREST.h>
#include <DHT.h>

#define DHTTYPE DHT11
#define dhtPin  D7
#define touchPin D5
#define buzzerPin D6
const int buzzerWait = 3000;

// --------------= VARIABLES =--------------
// WifiSetup parameters
const char* ssid = "TU_SSID";
const char* password = "TU_PASS";

// BroadcastUDP parameters
WiFiUDP UDP;
const char* udpIP = "10.0.0.17"; // La IP del dispositivo que escucha
//const char* udpIP = "192.168.1.133";
uint16_t udpPort = 1234;

// WebServerSetup parameters (& API)
WiFiServer server(80);
aREST rest = aREST();
String webString = "";     // String to display
int audio = 0;

// getWeather parameters
DHT dht(dhtPin, DHTTYPE, 11); // 11 works fine for ESP8266
float humidity, temperature;  // Values read from sensor
// Generally, you should use "unsigned long" for variables that hold time
unsigned long previousMillis = 0;        // will store last temp was read
const long interval = 2000;              // interval at which to read sensor


// --------------= MAIN =--------------
void setup() {
	// put your setup code here, to run once:
	Serial.begin(115200);
	Serial.println("\n\n");
	Serial.println("NodeMCU Rodolfo Server");
	Serial.println("Serial started!");
	pinMode(touchPin, INPUT);
	WifiSetup();
	WebServerSetup();
	buzzerBipBipBip(3);
}

void loop() {
	// put your main code here, to run repeatedly:
	APIResult();
	while (digitalRead(touchPin)) {
		BroadcastUDP();
	}
}


// --------------= FUNCTIONS =--------------
void WifiSetup() {
	int contconexion = 0;
	pinMode(5, OUTPUT);
	digitalWrite(15, LOW);

	WiFi.mode(WIFI_STA); // Para que no inicie el SoftAP en el modo normal
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED and contconexion < 50) { // Cuenta hasta 50 si no se puede conectar lo cancela
		++contconexion;
		delay(250);
		Serial.print(".");
		digitalWrite(5, HIGH);
		delay(250);
		digitalWrite(5, LOW);
	}
	if (contconexion < 50) {
		// Para usar con ip fija
		/*IPAddress Ip(192, 168, 1, 180);
		IPAddress Gateway(192, 168, 1, 1);
		IPAddress Subnet(255, 255, 255, 0);
		WiFi.config(Ip, Gateway, Subnet);*/

		Serial.println("");
		Serial.print("Connected to ");
		Serial.println(ssid);
		Serial.print("IP address: ");
		Serial.println(WiFi.localIP());
		digitalWrite(5, HIGH);
	}
	else {
		Serial.println("");
		Serial.println("Error de conexion");
		digitalWrite(15, HIGH);
	}
}

void BroadcastUDP() {
	UDP.beginPacket(udpIP, udpPort); // La ip del equipo donde se reproduce este script

	for (int i = 0; i < 1024;i++) {
		int old = micros();

		float analog = analogRead(17);

		analog = ((analog / 1) - 385);
		if (analog > 255) {
			analog = 255;
		}
		else if (analog < 0) {
			analog = 0;
		}

		UDP.write(int(analog));
		while (micros() - old < 87); // 90uSec = 1Sec/11111Hz - 3uSec para los otros procesos
	}
	UDP.endPacket();
	delay(1);
}

void getWeather() {
	// Wait at least 2 seconds seconds between measurements.
	unsigned long currentMillis = millis();

	if (currentMillis - previousMillis >= interval) {
		// save the last time you read the sensor 
		previousMillis = currentMillis;

		// Reading temperature for humidity takes about 250 milliseconds!
		// Sensor readings may also be up to 2 seconds 'old' (it's a very slow sensor)
		humidity = dht.readHumidity();          // Read humidity (percent)
		temperature = dht.readTemperature();     // Read temperature as Celsius
		// Check if any reads failed and exit early (to try again).
		if (isnan(humidity) || isnan(temperature)) {
			Serial.println("Failed to read from DHT sensor!");
			return;
		}
	}
}

// --------------= WebServer =--------------
void WebServerSetup() {
	dht.begin();           // initialize temperature sensor

	// Init variables and expose them to REST API
	rest.variable("temperature", &temperature);
	rest.variable("humidity", &humidity);
	rest.variable("audio", &audio);

	// Give name and ID to device
	rest.set_id("1");
	rest.set_name("sensor_module");

	server.begin();
	Serial.println("HTTP server started!");
}

void APIResult() {
	// Reading temperature and humidity
	//temperature = dht.readTemperature();
	//humidity = dht.readHumidity();
	getWeather();
	audio = digitalRead(touchPin);

	// Handle REST calls
	WiFiClient client = server.available();
	if (!client) {
		return;
	}
	while (!client.available()) {
		delay(1);
	}
	rest.handle(client);
}

// --------------= Audio =--------------
void buzzerBipBipBip(int i)
{
	for (i = 0; i < 3; i++) {
		tone(buzzerPin, 1000, 50);
		delay(150);
	}
}
