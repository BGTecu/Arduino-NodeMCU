#include <ESP8266WiFi.h>

#define relayPin D0
#define InternetOkPin D7
#define InternetErrorPin D8
#define buzzerPin D6
const int buzzerWait = 3000;


const char* ssid = "TU_SSID";
const char* password = "TU_PASS";

WiFiServer server(80);

// --------------= MAIN =--------------
void setup() {
	Serial.begin(115200); //Inicio el puerto serie

	pinMode(relayPin, OUTPUT);
	digitalWrite(relayPin, LOW);

	WifiSetup();
	WebServerSetup();
	buzzerBipBipBip();
}

void loop() {
	// Compruebo si hay un cliente disponible (una petici�n)
	WiFiClient client = server.available();
	if (!client) {
		return; // En caso de no haber un cliente, no hago nada
	}

	// Espero hasta que el cliente realice una petici�n
	Serial.println("�Nuevo cliente!");
	while (!client.available()) {
		delay(1);
	}

	WebSiteLoop(client);
}


// --------------= FUNCTIONS =--------------
void WifiSetup() {
	int contconexion = 0;
	pinMode(InternetOkPin, OUTPUT);
	pinMode(InternetErrorPin, OUTPUT);
	digitalWrite(InternetErrorPin, LOW);

	WiFi.mode(WIFI_STA); // Para que no inicie el SoftAP en el modo normal
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED and contconexion < 50) { // Cuenta hasta 50 si no se puede conectar lo cancela
		++contconexion;
		delay(250);
		Serial.print(".");
		digitalWrite(InternetOkPin, HIGH);
		delay(250);
		digitalWrite(InternetOkPin, LOW);
	}
	if (contconexion < 50) {
		// Para usar con ip fija
		/*IPAddress Ip(192, 168, 1, 180);
		IPAddress Gateway(192, 168, 1, 1);
		IPAddress Subnet(255, 255, 255, 0);
		WiFi.config(Ip, Gateway, Subnet);*/

		Serial.print("\n\nConnected to ");
		Serial.println(ssid);
		Serial.print("IP address: ");
		Serial.println(WiFi.localIP());
		digitalWrite(InternetOkPin, HIGH);
	}
	else {
		Serial.println("\nError de conexion");
		digitalWrite(InternetErrorPin, HIGH);
	}
}

// --------------= WebServer =--------------
void WebServerSetup() {
	server.begin();
	Serial.println("HTTP server started!");
}

void WebSiteLoop(WiFiClient client)
{
	// Leo la primera linea de la petici�n del cliente
	String request = client.readStringUntil('\r'); // Leo hasta retorno de carro
	Serial.println(request); //Imprimo la petici�n
	client.flush(); //Limpio el buffer

	// Interpreto lo que he recibido

	int value = LOW;
	if (request.indexOf("/RELAY=ON") != -1) {
		digitalWrite(relayPin, HIGH);
		value = HIGH;
	}
	if (request.indexOf("/RELAY=OFF") != -1) {
		digitalWrite(relayPin, LOW);
		value = LOW;
	}

	// Pongo ledPin al valor que ha solicitado el cliente en la petici�n

	  // Devuelvo la respuesta al cliente -> Todo ha ido bien, el mensaje ha sido interpretado correctamente
	client.println("HTTP/1.1 200 OK");
	client.println("Content-Type: text/html");
	client.println(""); //  do not forget this one

	// A partir de aqu� creo la p�gina en raw HTML
	client.println("<!DOCTYPE HTML>");
	client.println("<html>");

	client.print("The RELAY is:  ");
	if (value == HIGH) {
		client.print("ON");
	}
	else {
		client.print("OFF");
	}
	client.println("<br><br>");
	client.println("<a href=\"/RELAY=ON\"\"><button>Encender </button></a>"); // Los botones con enlace
	client.println("<a href=\"/RELAY=OFF\"\"><button>Apagar </button></a><br />");
	client.println("</html>");
}

void buzzerBipBipBip()
{
	int i;
	for (i = 0; i < 3; i++) {
		tone(buzzerPin, 1000, 50);
		delay(150);
	}
}
